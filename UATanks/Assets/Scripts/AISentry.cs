﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISentry : MonoBehaviour {

    //Create enum for AI state
    public enum AIState { Patrol, LookFor, ChaseAndFire };
    public AIState aiState = AIState.Patrol;

    //Create variable for timer
    public float stateEnterTime;
    //Create variable for how close player has to be for AI to "hear" 
    public float aiSenseRadius;
    public float restHealRate; //in hp per second

    //Create variables for tank components 
    private Transform tf;
    private TankData data;
    private TankMotor motor;

    //Create variable for target tank transform
    private Transform target;


    //Create variable for avoidance stage index 
    private int avoidanceStage = 0;
    //Create variables for avoidance timer
    public float avoidanceTime = 2.0f;
    private float exitTime;

    //Create bool to check for direction tank is traveling through waypoints
    private bool isPatrolForward = true;

    //Create array for waypoints transforms
    public Transform[] waypoints;
    //Create variable to hold current waypoint
    private int currentWaypoint = 0;
    //Create variable to hold distance that is "close enough" to waypoint & can be changed by designer
    public float closeEnough = 1.0f;

    //Create variable for distance to flee
    public float fleeDistance = 1.0f;

    //Create variable for Shooter script
    private Shooter shooter;
    //Create timer to track time before next allowed shot
    private float lastShootTime = 0;


    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
        motor = GetComponent<TankMotor>();
        shooter = GetComponent<Shooter>();
        //Assign player as target
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update () {
        if (aiState == AIState.Patrol)
        {
            //Perform behaviors
            if (avoidanceStage != 0) //If there is an obstacle
            { //Start avoidance
                DoAvoidance();
            }
            else
            { //If no obstacles, patrol through waypoints
                DoPatrol();
            }

            //Check for transitions
            //If "hears" player, 
            if (Vector3.SqrMagnitude(tf.position - target.position) <= (aiSenseRadius * aiSenseRadius))
            {  //Change state to "Look for"
                ChangeState(AIState.LookFor);
            } 
        }
        else if (aiState == AIState.LookFor)
        {
            //Perform behaviors
            DoLookFor();

            //Check for transitions
            if (CanSee(target))
            {//Fire on player
                ChangeState(AIState.ChaseAndFire);
            }
            else
            {//Otherwise, go back to "Patrol" state
                ChangeState(AIState.Patrol);
            }
        }
        else if (aiState == AIState.ChaseAndFire)
        {
            //Perform behaviors
            if (avoidanceStage != 0) //If there is an obstacle
            { //Start avoidance
                DoAvoidance();
            }
            else
            {
                DoChase();

                //Limit firing rate, so ai can only shoot if enough time has passed
                if (Time.time > lastShootTime + data.fireDelay)
                { //Shoot at player
                    shooter.ShootCannon(data.shotForce, data.shellDestruct);
                    //reset timer
                    lastShootTime = Time.time;
                }
            }

        //Check for transitions
        //If player moves too far away,
        if (Vector3.SqrMagnitude(tf.position - target.position) > (aiSenseRadius * aiSenseRadius))
            {//Go back to patrolling
                ChangeState(AIState.Patrol);
            }
        }
    }


    public void ChangeState(AIState newState)
    {
        //Change ai state
        aiState = newState;

        //Save the time we changed states
        stateEnterTime = Time.time;
    }

    public bool CanSee(Transform target)
    {
        //Find vector from ai to target
        Vector3 aiToTarget = target.position - tf.position;

        //Find the angle between the direction ai is facing and our target
        float angleToTarget = Vector3.Angle(aiToTarget, tf.forward);
        //if angle is less than our FOV
        if (angleToTarget < data.fieldOfView)
        {//Raycast to see if line of sight is clear
            RaycastHit hit;
            if (Physics.Raycast(tf.position, tf.forward, out hit))
            {//If raycast hits a collider that does not belong to player, something is blocking line of sight
                if (!hit.collider.CompareTag("Player"))
                {
                    return false;
                }
            }
        }
        return true; 
    }

    public bool CanMove(float speed)
    {
        //Cast a ray forward in the distance that we sent in
        RaycastHit hit;
        //If our raycast hit something,
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {//besides the player
            if (!hit.collider.CompareTag("Player"))
            {
                return false;
            }
        }
        //Otherwise, we can move
        return true;
    }

    public void DoAvoidance()
    //Handles obstacle avoidance
    {
        if (avoidanceStage == 1)
        {
            //Rotate left
            motor.Turn(-1 * data.turnSpeed);
            //Check to see if turning cleared the way
            if (CanMove(data.moveSpeed))
            {
                avoidanceStage = 2;

                //Set the timer for the number of seconds to stay in stage 2
                exitTime = avoidanceTime;
            }
            //Otherwise, we'll do this again next turn!
        }
        else if (avoidanceStage == 2)
        {
            //If we can move forward, do so
            if (CanMove(data.moveSpeed))
            {
                //Subtract from timer and move
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);

                //If we have moved for a long enough time, return to chase
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                //Otherwise, can't move forward, so go back to stage 1
                avoidanceStage = 1;
            }
        }
    }
    public void DoPatrol ()
        { //Recover health if needed during patrol
        //Increase ai health in hp per second
        data.health += restHealRate * Time.deltaTime;

        //...but don't go over max health
        data.health = Mathf.Min(data.health, data.maxHealth);

        //Check if already rotated towards waypoint
        if (motor.RotateTowards(waypoints[currentWaypoint].position, data.turnSpeed))
        {
            //Do nothing! (the tank is turned already where you want it)
        }
        else
        {
            DoAvoidance();
            //Move forward
            motor.Move(data.moveSpeed);
        }
        //If we are close to the waypoint
        //Changed Vector3.Distance to squaring each side for optimization
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {
            if (isPatrolForward)
                { //Check if going forward
                    //Advance to next waypoint, 
                    if (currentWaypoint < waypoints.Length - 1)
                    {
                        currentWaypoint++;
                    }
                    else
                    { //Otherwise reverse direction
                        isPatrolForward = false;
                        currentWaypoint--;
                    }
                }
                else
                { //Not going forward
                    if (currentWaypoint > 0)
                    {
                        //Advance to next waypoint going "backward"
                        currentWaypoint--;
                    }
                    else
                    {
                        //When reach waypoint 0, reverse direction
                        isPatrolForward = true;
                        currentWaypoint++;
                    }

                }
            }
        }

    public void DoLookFor()
    {
        //Rotate towards target
        motor.RotateTowards(target.position, data.turnSpeed);
    }

    public void DoChase()
    {
        motor.RotateTowards(target.position, data.turnSpeed);
        //Check if we can move "data.moveSpeed" units away
        //  This distance allows us to look for collisions 1 second into the future
        if (CanMove(data.moveSpeed))
        {
            motor.Move(data.moveSpeed);
        }
        else
        {
            //Can't move, so enter obstacle avoidance stage 1
            avoidanceStage = 1;
        }
    }
}

