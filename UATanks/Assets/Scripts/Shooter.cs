﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{

    //create variable for prefab shells
    public GameObject cannonShell;

    //create variable for TankData component
    private TankData data;
    //variable for point to fire shells from
    public Transform firePoint;
    //Create variable for shooter's transform
    private Transform tf;

    

    // Use this for initialization
    void Start()
    {
        //Set variable for Transform component
        tf = GetComponent<Transform>();
        //Set variable for TankData component
        data = gameObject.GetComponent<TankData>();
    }

    

    public void ShootCannon(float force, float destruct)
    {
        AudioSource.PlayClipAtPoint(AudioList.instance.fire, tf.position, AudioList.instance.sfxVol);

        //Create shell and store it in a variable
        GameObject shell;

        //Instantiate shell
        shell = Instantiate(cannonShell, firePoint.position, Quaternion.identity) as GameObject;

        //create new Shell object variable called shellData
        Shell shellData;
        //get Shell component from object we just instantiated
        shellData = shell.GetComponent<Shell>();
        //set the shooter variable in that shellData to the data from the Shooter object
        shellData.shooter = data;

        //access rigidbody component of shell
        Rigidbody tempRigidbody;
        tempRigidbody = shell.GetComponent<Rigidbody>();

        //add force vector to rigidbody of shell
        tempRigidbody.AddForce(force * firePoint.forward);

        //have shells self-destruct after time
        Destroy(shell, destruct);
    }

    
}
