﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    //Create enum for Input Schemes, choice between WASD and arrow key inputs
    public enum InputScheme { WASD, arrowKeys };
    //Create variable for Input Schemes
    public InputScheme input = InputScheme.arrowKeys;


    //Create variable for access to TankMotor script
    public TankMotor motor;
    //Create variable for access to TankData script
    public TankData data;
    //Create variable for access to Shooter script
    public Shooter shooter;

    //Create timer to track time before next shot allowed
    private float delayTimer = 0;



    
	
	// Update is called once per frame
	void Update () {
        //start timer 
        delayTimer -= Time.deltaTime;

        /*if (Input.GetKey(KeyCode.Space) && delayTimer <= 0)
        {
            //If spacebar is pressed and enough time has passed, shoot 
            Debug.Log("Bang!");
            shooter.ShootCannon(data.shotForce, data.shellDestruct);
            //set delay on timer
            delayTimer = data.fireDelay;
        }*/

        //set up switch to handle user inputs for tank movement
        switch (input) {
            //Check for arrow key inputs if this scheme is chosen
            case InputScheme.arrowKeys:
                if (Input.GetKey(KeyCode.UpArrow)) //move forward
                {
                    motor.Move(data.moveSpeed);
                }
                if (Input.GetKey(KeyCode.DownArrow)) //move backward
                {
                    motor.Move(-data.moveSpeed);
                }
                if (Input.GetKey(KeyCode.RightArrow)) //rotate clockwise
                {
                    motor.Turn(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.LeftArrow)) //rotate counterclockwise
                {
                    motor.Turn(-data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.Space) && delayTimer <= 0) //shoot cannon 
                {
                    //If spacebar is pressed and enough time has passed, shoot 
                    Debug.Log("Bang!");
                    shooter.ShootCannon(data.shotForce, data.shellDestruct);
                    //set delay on timer
                    delayTimer = data.fireDelay;
                }
                break;

            //Check for WASD inputs if this scheme is chosen
            case InputScheme.WASD:
                if (Input.GetKey(KeyCode.W))
                {
                    motor.Move(data.moveSpeed); //move forward
                }
                if (Input.GetKey(KeyCode.S))
                {
                    motor.Move(-data.moveSpeed); //move backward
                }
                if (Input.GetKey(KeyCode.D))
                {
                    motor.Turn(data.turnSpeed); //rotate clockwise
                }
                if (Input.GetKey(KeyCode.A))
                {
                    motor.Turn(-data.turnSpeed); //rotate counterclockwise
                }
                if (Input.GetKey(KeyCode.LeftControl) && delayTimer <= 0) //shoot cannon
                {
                    //If spacebar is pressed and enough time has passed, shoot 
                    Debug.Log("Bang!");
                    shooter.ShootCannon(data.shotForce, data.shellDestruct);
                    //set delay on timer
                    delayTimer = data.fireDelay;
                }
                break;
        }
    }
}
