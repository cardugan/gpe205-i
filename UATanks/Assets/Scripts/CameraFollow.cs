﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    //Create variable for target for camera to follow
    public GameObject target;
    Vector3 offset; //Create variable for a vector to position the camera
    private Transform tf; //Create variable for camera's transform

    // Runs before Start ()
    void Awake () {
        //Set variable for Transform component
        tf = GetComponent<Transform>();
    }

	// Use this for initialization
	void Start () {
        //Set vector from camera to target
        offset = target.transform.position - tf.position;
	}
	
	
    // Runs after Update (), waits for target to reach position at end of update then follows
    void LateUpdate () {
        //Orient camera behind target
        float desiredAngle = target.transform.eulerAngles.y;
        Quaternion rotation = Quaternion.Euler(0, desiredAngle, 0);
        tf.position = target.transform.position - (rotation * offset);
        //Have camera look at the target
        tf.LookAt(target.transform);
    }
}
