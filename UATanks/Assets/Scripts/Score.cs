﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    //Created variables for text for score and lives
    public Text scoreText;
    public Text livesText;
    

    //Create variable for player tank data
    public TankData player;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        scoreText.text = "SCORE: " + player.score;
        livesText.text = "LIVES: " + player.numLives;
	}

}
