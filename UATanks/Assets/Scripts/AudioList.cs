﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioList : MonoBehaviour {

    //Create singleton audio list
    public static AudioList instance;

    //Create variable for audio clips
    public AudioClip powerupPickup;
    public AudioClip shellHit;
    public AudioClip fire;
    public AudioClip tankDeath;
    public AudioClip alert;

    //Create variable for sfx volume
    public float sfxVol;

    //Create variable for audio mixer
    public AudioMixer sfxMixer;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Awake () {
		if (instance == null)
        {
            instance = this;
        } else
        {
            Destroy(this);
        }
	}
}
