﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollision : MonoBehaviour {

    //detect and destroy gameobjects on collision
    void OnCollisionEnter(Collision collision)
    {
        //destroy self if collide with other collider
        Destroy(gameObject);
    }
}
