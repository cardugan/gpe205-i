﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour {

    //Create variable for TankData component of tank shooting this shell
    public TankData shooter;
    //Create variable for shell game object
    public GameObject cannonShell;

    

    void OnCollisionEnter(Collision collision)
    {
        AudioSource.PlayClipAtPoint(AudioList.instance.shellHit, transform.position, AudioList.instance.sfxVol);

        //Check to see if shell hit a tank 
        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Enemy")
        {
            //Show where this shell came from through TankData component
            Debug.Log("Hit by " + shooter.gameObject.name);
            //Decrease player or enemy tank's health by damage done by shooter
            collision.gameObject.GetComponent<TankData>().health -= shooter.damage;
            shooter.score += shooter.ptsPerShot;
            if (collision.gameObject.GetComponent<TankData>().numLives <= 0)
            {
                shooter.score += shooter.ptsPerKill;
            }
        }
    }
}
