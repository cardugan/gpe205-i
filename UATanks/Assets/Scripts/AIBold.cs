﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBold : MonoBehaviour {

    //Create enum for AI state
    public enum AIState { Idle, ChaseAndFire, Flee, Rest };
    public AIState aiState = AIState.Idle;

    //Create variable for timer
    public float stateEnterTime;
    //Create variable for how close player has to be for AI to "hear" 
    public float aiSenseRadius;
    public float restHealRate; //in hp per second

    //Create variables for tank components 
    private Transform tf;
    private TankData data;
    private TankMotor motor;

    //Create variable for target tank transform
    private Transform target;

    //Create variable for avoidance stage index 
    private int avoidanceStage = 0;
    //Create variables for avoidance timer
    public float avoidanceTime = 2.0f;
    private float exitTime;

    //Create variable for distance to flee
    public float fleeDistance = 1.0f;

    //Create variable for Shooter script
    private Shooter shooter;
    //Create timer to track time before next allowed shot
    private float lastShootTime = 0;


    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
        motor = GetComponent<TankMotor>();
        shooter = GetComponent<Shooter>();
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (aiState == AIState.Idle)
        {
            //Perform behaviors
            DoIdle();

            //Check for transitions
            //If hp drops below one third of max
            if (data.health < data.maxHealth * 0.3f)
            { //Change to "CheckForFlee" state
                ChangeState(AIState.Flee);
            }
            //Or if player is within "hearing" distance
            else if (Vector3.SqrMagnitude(tf.position - target.position) <= (aiSenseRadius * aiSenseRadius))
            { //Change to "ChaseAndFire" state
                ChangeState(AIState.ChaseAndFire);
            }
        }
        else if (aiState == AIState.ChaseAndFire)
        {
            //Perform behaviors
            if (avoidanceStage != 0) //If there is an obstacle
            { //Start avoidance
                DoAvoidance();
            }
            else
            {
                DoChase();

                //Limit firing rate, so ai can only shoot if enough time has passed
                if (Time.time > lastShootTime + data.fireDelay)
                { //Shoot at player
                    shooter.ShootCannon(data.shotForce, data.shellDestruct);
                    //reset timer
                    lastShootTime = Time.time;
                }
            }

            //Check for transitions
            //If hp drops below half of max
            if (data.health < data.maxHealth * 0.3f)
            { //Change to "Flee" state
                ChangeState(AIState.Flee);
            }
            //Or if player moves outside of "hearing" distance
            else if (Vector3.SqrMagnitude(tf.position - target.position) > (aiSenseRadius * aiSenseRadius))
            { //Change back to "Chase" state
                ChangeState(AIState.Idle);
            }
        }
        
        else if (aiState == AIState.Flee)
        {
            //Perform behaviors
            if (avoidanceStage != 0) //If there is an obstacle
            { //Start avoidance
                DoAvoidance();
            }
            else
            {
                DoFlee();
            }

            //Check for transitions
            //If player is outside of "hearing" distance
            if (Vector3.SqrMagnitude(tf.position - target.position) > (aiSenseRadius * aiSenseRadius))
            {
                ChangeState(AIState.Rest);
            }
        }
        else if (aiState == AIState.Rest)
        {
            //Perform behaviors
            DoRest();

            //Check for transitions
            //If player is within "hearing" distance: too close
            if (Vector3.SqrMagnitude(tf.position - target.position) <= (aiSenseRadius * aiSenseRadius))
            { //Change to "Flee" state
                ChangeState(AIState.Flee);
            }
            //Or if hp recovered to max
            else if (data.health >= data.maxHealth)
            { //Change to "Idle" state
                ChangeState(AIState.Idle);
            }
        }
    }

    public void DoIdle()
    {
        //Do nothing
    }


    public void DoFlee()
    {
        Vector3 vectorToTarget = target.position - tf.position;

        //To get the vector AWAY from target, multiply by -1
        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

        //Normalize to give it a magnitude of 1
        vectorAwayFromTarget.Normalize();
        //A normalized vector can be multiplied by a length to make a vector of that length
        vectorAwayFromTarget *= fleeDistance;

        //Now add vectorAwayFromTarget to AI's position to get a point to flee to
        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        motor.RotateTowards(fleePosition, data.turnSpeed);
        motor.Move(data.moveSpeed);
    }

    public void DoRest()
    {
        //Increase ai health in hp per second
        data.health += restHealRate * Time.deltaTime;

        //...but don't go over max health
        data.health = Mathf.Min(data.health, data.maxHealth);

    }

    public void ChangeState(AIState newState)
    {
        //Change ai state
        aiState = newState;

        //Save the time we changed states
        stateEnterTime = Time.time;
    }

    public bool CanMove(float speed)
    {
        //Cast a ray forward in the distance that we sent in
        RaycastHit hit;
        //If our raycast hit something,
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {//besides the player
            if (!hit.collider.CompareTag("Player"))
            {
                return false;
            }
        }
        //Otherwise, we can move
        return true;
    }

    public void DoAvoidance()
    //Handles obstacle avoidance
    {
        if (avoidanceStage == 1)
        {
            //Rotate left
            motor.Turn(-1 * data.turnSpeed);
            //Check to see if turning cleared the way
            if (CanMove(data.moveSpeed))
            {
                avoidanceStage = 2;

                //Set the timer for the number of seconds to stay in stage 2
                exitTime = avoidanceTime;
            }
            //Otherwise, we'll do this again next turn!
        }
        else if (avoidanceStage == 2)
        {
            //If we can move forward, do so
            if (CanMove(data.moveSpeed))
            {
                //Subtract from timer and move
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);

                //If we have moved for a long enough time, return to chase
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                //Otherwise, can't move forward, so go back to stage 1
                avoidanceStage = 1;
            }
        }
    }

    public void DoChase()
    {
        motor.RotateTowards(target.position, data.turnSpeed);
        //Check if we can move "data.moveSpeed" units away
        //  This distance allows us to look for collisions 1 second into the future
        if (CanMove(data.moveSpeed))
        {
            motor.Move(data.moveSpeed);
        }
        else
        {
            //Can't move, so enter obstacle avoidance stage 1
            avoidanceStage = 1;
        }
    }
}
