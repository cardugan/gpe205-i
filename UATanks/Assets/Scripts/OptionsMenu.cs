﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using System;

public class OptionsMenu : MonoBehaviour {


    //Create variable for audio mixer
    public AudioMixer musicMixer;
    public AudioMixer SFXMixer;
    //Create variables for sliders
    public Slider SFXSlider;
    public Slider musicSlider;

    public void SetSFXVolume (float volume)
    {
        SFXMixer.SetFloat("SFXVolume", Mathf.Log10(AudioList.instance.sfxVol) * 20);
    }

    //Function to set music volume to audioMixer volume
    public void SetMusicVolume (float volume)
    {
        musicMixer.SetFloat("MusicVolume", Mathf.Log10(volume)*20);
    }

    void OnDisable()
    {
        //Create variables for volume settings
        float SFXVolume;
        float musicVolume;

        //Get volumes from audioMixer
        SFXMixer.GetFloat("SFXVolume", out SFXVolume);
        musicMixer.GetFloat("MusicVolume", out musicVolume);

        //Set and save PlayerPrefs for volume
        PlayerPrefs.SetFloat("SFXVolume", SFXVolume);
        PlayerPrefs.SetFloat("MusicVolume", musicVolume);
        PlayerPrefs.Save();
    }

   

}

