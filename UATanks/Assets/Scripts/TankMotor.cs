﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour {

    //Create a variable for the character controller component
    private CharacterController characterController;
    //Create a variable for the transform component
    private Transform tf;
    //Create a variable to contain to data for this tank
    private TankData data;

	// Use this for initialization
	void Start () {
        //Store our character controller in the variable
        characterController = gameObject.GetComponent<CharacterController>();
        //Store the transform of this game object in the variable
        tf = gameObject.GetComponent<Transform>();
        //Store the data of this game object in the variable
        data = gameObject.GetComponent<TankData>();
	}
	
	

    //This function moves our tank forward
    public void Move (float speed)
    {
        //Create a vector to hold our speed data
        Vector3 speedVector;

        //Start with the vector pointing the same direction as the game object this script is on
        speedVector = tf.forward;

        //Now, instead of our vector being one unit in length, apply our speed value
        speedVector *= speed;

        //Call SimpleMove() and send it to our vector
        //Note that SimpleMove() will apply Time.deltaTime and convert to meters per second for us
        characterController.SimpleMove(speedVector);
    }

    //This function rotates our tank
    public void Turn (float speed)
    {
        //Create a vector to hold rotation data
        Vector3 rotateVector;

        //Start by rotating right by one degree per frame draw (left is negative right)
        rotateVector = Vector3.up;

        //Adjust our rotation based on speed
        rotateVector *= speed * Time.deltaTime;

        //Transform.Rotate() doesn't account for speed, so we need to change from per frame to per second
        //rotateVector *= Time.deltaTime; (combined with statement above)

        //Now rotate our tank by this value - in local space, not world space
        tf.Rotate(rotateVector, Space.Self);
    }

    //This function rotates towards target if possible
    public bool RotateTowards(Vector3 target, float speed) {
        //return true if able to rotate, return false if already rotated towards target

        //difference between current position and target
        Vector3 vectorToTarget = target - tf.position;

        //find rotation that looks down the vector
        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);
        //if already turned that direction
        if (targetRotation == tf.rotation)
        {
            //do not need to turn
            return false;
        }

        //otherwise make the rotation using our turnSpeed and Time.deltaTime for degrees per second
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, data.turnSpeed * Time.deltaTime);

        //we rotated so... 
        return true;
    }
}
