﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankData : MonoBehaviour {


    //Public data for easy access to designers

    public float moveSpeed = 3f; //in meters per second
    public float turnSpeed = 90f; //in degrees per second
    public float shotForce = 1000f; //force of projectile
    public float fireDelay = 0.5f; //delay between shots fired
    public float shellDestruct = 3f; //time until shells self-destruct
    public float damage = 20f; //health point damage to other tank per shot
    public int ptsPerShot = 10; //score per hit
    public int ptsPerKill = 100; //score per destruction of enemy tank

    public float maxHealth = 100f; //maximum health points
    public float health; //current health points
    public int numLives = 1; //current number of lives before game over
    public int maxLives = 1; //number of lives to start with
    public int score = 0; //player score

    public float fieldOfView = 45f;


    //Create bool for player death
    public bool playerIsDead = false;

	// Use this for initialization
	void Start () {
        //Set initial health and numLives
        health = maxHealth;
        numLives = maxLives;
    }

    // Update is called once per frame
    void Update () {
        if (gameObject.CompareTag("Enemy")) { 
		    if (health <= 0) {
                AudioSource.PlayClipAtPoint(AudioList.instance.tankDeath, transform.position, 
                    AudioList.instance.sfxVol);
                Destroy(gameObject, 1.0f * Time.deltaTime);
               
                //respawn enemy
                //create random integer to represent spawnpoint array index
                int index = GameManager.instance.RandomIndex();
                GameObject enemy;
                enemy = Instantiate (this.gameObject, GameManager.instance.enemySpawnPoints[index]) as GameObject;
            }
        }
        else if (gameObject.CompareTag ("Player"))
        {
            if (health <= 0)
            {
                if (numLives >= 1)
                {
                    numLives--;
                    health += maxHealth;
                }
               
            }

            if (numLives <= 0)
            {
                health = 0;
                playerIsDead = true;
                if (GameManager.instance.isTwoPlayerGame)
                {
                    gameObject.GetComponent<InputManager>().enabled = false;
                    if (GameManager.instance.playerTwoData.playerIsDead && 
                        GameManager.instance.playerOneData.playerIsDead)
                    {
                        GameManager.instance.GameOver();
                    }
                }
                else
                {
                    GameManager.instance.GameOver();
                }
            }
        }
	}
}
