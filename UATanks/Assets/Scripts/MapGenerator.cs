﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour {

    //Create variables for number of rows and columns for rooms in map to be generated
    public int rows;
    public int cols;
    //Create variables for our 50 x 50 room tiles
    private float roomWidth = 50.0f;
    private float roomHeight = 50.0f;

    //Create a 2D array to store grid
    private Room[,] grid;
    //Create an array for room prefabs
    public GameObject[] gridPrefabs;




	// Use this for initialization
	void Start () {
        //Generate grid
        GenerateGrid();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //Returns a random room
    public GameObject RandomRoomPrefab ()
    {
        return gridPrefabs[Random.Range(0, gridPrefabs.Length)];
    }

    public void GenerateGrid ()
    {
        //Clear out the grid
        grid = new Room[cols, rows];

        //For each grid row...
        for (int i = 0; i < rows; i++)
        {
            //For each column in that row
            for (int j = 0; j < cols; j++)
            {
                //Figure out the location
                float xPosition = roomWidth * j;
                float zPosition = roomHeight * i;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);

                //Create a new grid at the appropriate location
                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, 
                    Quaternion.identity) as GameObject;

                //Set its parent
                tempRoomObj.transform.parent = this.transform;

                //Give it a meaningful name
                tempRoomObj.name = "Room_" + j + "," + i;

                //Get the room object
                Room tempRoom = tempRoomObj.GetComponent<Room>();

                //Open the doors
                //If we are on the bottom row, open the north door
                if (i == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }//Or if we are on the top row, open the south door
                else if (i == rows - 1)
                {
                    tempRoom.doorSouth.SetActive(false);
                }//Otherwise we are in the middle, open both doors
                else
                {
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }
                //If we are on the first column, open the east door
                if (j == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                }//Or if we are on the last column, open the west door
                else if (j == cols -1)
                {
                    tempRoom.doorWest.SetActive(false);
                }//Otherwise we are in the middle, open both doors
                else
                {
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }

                //Save it to the grid array
                grid[j, i] = tempRoom;
            }
        }
    }
}
