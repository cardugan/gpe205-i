﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIHelper : MonoBehaviour {

    //Create enum for AI state
    public enum AIState { Chase, ChaseAndFire };
    public AIState aiState = AIState.Chase;


    //Create variable for timer
    public float stateEnterTime;
    //Create variable for how close player has to be for AI to "hear" 
    public float aiSenseRadius;

    //Create variables for tank components 
    private Transform tf;
    private TankData data;
    private TankMotor motor;

    //Create variable for target tank transform
    private Transform target;

    //Create variable for avoidance stage index 
    private int avoidanceStage = 0;
    //Create variables for avoidance timer
    public float avoidanceTime = 2.0f;
    private float exitTime;


    //Create variable for Shooter script
    private Shooter shooter;
    //Create timer to track time before next allowed shot
    private float lastShootTime = 0;


    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
        motor = GetComponent<TankMotor>();
        shooter = GetComponent<Shooter>();
        //Assign player as target 
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

    }

    // Update is called once per frame
    void Update()
    {
        if (aiState == AIState.Chase)
        {
            //Perform behaviors
            if (avoidanceStage != 0) //If there is an obstacle
            { //Start avoidance
                DoAvoidance();
            }
            else
            { //If no obstacles, chase player
                DoChase();
            }

            //Check for transitions
            //If player is within "hearing" distance
            if (Vector3.SqrMagnitude(tf.position - target.position) <= (aiSenseRadius * aiSenseRadius))
            { //Change to "ChaseAndFire" state
                ChangeState(AIState.ChaseAndFire);
            }
        }
        else if (aiState == AIState.ChaseAndFire)
        {
            //Perform behaviors
            if (avoidanceStage != 0) //If there is an obstacle
            { //Start avoidance
                DoAvoidance();
            }
            else
            {
                DoChase();

                //Limit firing rate, so ai can only shoot if enough time has passed
                if (Time.time > lastShootTime + data.fireDelay)
                { //Shoot at player
                    shooter.ShootCannon(data.shotForce, data.shellDestruct);
                    //reset timer
                    lastShootTime = Time.time;
                }
            }

            //Check for transitions
            //If player moves outside of "hearing" distance
            if (Vector3.SqrMagnitude(tf.position - target.position) > (aiSenseRadius * aiSenseRadius))
            { //Change back to "Chase" state
                ChangeState(AIState.Chase);
            }
        }
        
    }

    public void ChangeState(AIState newState)
    {
        //Change ai state
        aiState = newState;

        //Save the time we changed states
        stateEnterTime = Time.time;
    }

    public bool CanMove(float speed)
    {
        //Cast a ray forward in the distance that we sent in
        RaycastHit hit;
        //If our raycast hit something,
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {//besides the player
            if (!hit.collider.CompareTag("Player"))
            {
                return false;
            }
        }
        //Otherwise, we can move
        return true;
    }

    public void DoAvoidance()
    //Handles obstacle avoidance
    {
        if (avoidanceStage == 1)
        {
            //Rotate left
            motor.Turn(-1 * data.turnSpeed);
            //Check to see if turning cleared the way
            if (CanMove(data.moveSpeed))
            {
                avoidanceStage = 2;

                //Set the timer for the number of seconds to stay in stage 2
                exitTime = avoidanceTime;
            }
            //Otherwise, we'll do this again next turn!
        }
        else if (avoidanceStage == 2)
        {
            //If we can move forward, do so
            if (CanMove(data.moveSpeed))
            {
                //Subtract from timer and move
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);

                //If we have moved for a long enough time, return to chase
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                //Otherwise, can't move forward, so go back to stage 1
                avoidanceStage = 1;
            }
        }
    }

    public void DoChase()
    {
        motor.RotateTowards(target.position, data.turnSpeed);
        //Check if we can move "data.moveSpeed" units away
        //  This distance allows us to look for collisions 1 second into the future
        if (CanMove(data.moveSpeed))
        {
            motor.Move(data.moveSpeed);
        }
        else
        {
            //Can't move, so enter obstacle avoidance stage 1
            avoidanceStage = 1;
        }
    }
    
}
