﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour {

    public static GameManager instance; //Create static variable for our single GameManager

    
    //create enum for choice of map generation
    public enum MapType { RandomLevel, MapOfTheDay, PresetSeed };
    [Header("Map info")]
    public MapType map = MapType.RandomLevel;
    //create variable to allow random seed (editable)
    public int mapSeed;
    //Create variables for number of rows and columns for rooms in map to be generated
    public int rows;
    public int cols;
    //Create variables for our 50 x 50 room tiles
    private float roomWidth = 50.0f;
    private float roomHeight = 50.0f;

    //Create a 2D array to store grid
    private Room[,] grid;
    //Create an array for room prefabs
    public GameObject[] gridPrefabs;

    [Header("Tank info")]
    //How many enemy tanks on the field at once
    public int totalEnemyTanks = 4;
    //array to hold enemy tanks
    public GameObject[] enemyTanks;
    //array to hold enemy spawnpoints
    public Transform[] enemySpawnPoints;
    //array to hold player spawnpoints
    public Transform[] playerSpawnPoints;
    //list of index of spawnpoints
    List<int> SpawnIndex;
    //create list of instantiated enemy tanks
    public List<TankData> currentEnemyData;

    public GameObject playerOnePrefab;
    public GameObject playerTwoPrefab;

    public TankData playerOneData;
    public TankData playerTwoData;

    //Create a list of pickups spawned
    public List<Pickup> spawnedPickups;

    //Create bool for two player game
    public bool isTwoPlayerGame = false;

    //Create variables for player cameras
    public Camera camera1;
    public Camera camera2;

    //Create variable for Game Over screen
    public GameObject GameOverScreen;

    // Runs before any Start () functions run
    void Awake () {
        //Make sure only one GameManager exists
        if (instance == null) //If no GameManager yet, make this one
        {
            instance = this;
        }
        else //If one already exists, send error message and destroy this one
        {
            Debug.LogError("ERROR: There can be only one GameManager.");
            Destroy(gameObject);
        }
    }

    public void SetMapToRandom ()
    {
        map = MapType.RandomLevel;
    }

    public void SetMapToDay ()
    {
        map = MapType.MapOfTheDay;
    }

	// Use this for initialization
	void Start () {

        //set up switch to handle choice for map generator
        switch (map)
        {
            case MapType.RandomLevel:
                mapSeed = DateToInt(DateTime.Now);
                break;
            case MapType.MapOfTheDay:
                mapSeed = DateToInt(DateTime.Now.Date);
                break;
            case MapType.PresetSeed:
                mapSeed = 0;
                break;
        }

        //Initiate list of current enemies' tank data
        currentEnemyData = new List<TankData>();

        //Generate grid
        GenerateGrid();
        //spawn enemy tanks
        SpawnEnemies();
        //spawn player(s)
        SpawnPlayerOne();
        //If two player game chosen, spawn another player
        if (isTwoPlayerGame)
        {
            SpawnPlayerTwo();
 
        }

        //Initialize powerup list
        spawnedPickups = new List<Pickup>();
    }


// Update is called once per frame
void Update () {
        //Respawn enemy tanks as they are destroyed
        if (currentEnemyData.Count < totalEnemyTanks)
        {
            RespawnEnemy();
        }

        //Check for player death
		if (isTwoPlayerGame)
        {
            if (playerOneData.playerIsDead && playerTwoData.playerIsDead)
            {
                GameOver();
            }
            
        }
        else 
        {
            if (playerOneData.playerIsDead)
            {
                Debug.Log("Game Over");
                GameOver();
            }
        }
	}

    /*  ================================================================================
	 * Map Generator functions
	 * =================================================================================
	 */

    //Returns a random room
    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
    }

    public int DateToInt(DateTime dateToUse)
    {
        //Add our date up and return it
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour +
            dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }

    public void GenerateGrid()
    {
        //set random seed
        UnityEngine.Random.InitState(mapSeed);

        //Clear out the grid
        grid = new Room[cols, rows];

        //For each grid row...
        for (int i = 0; i < rows; i++)
        {
            //For each column in that row
            for (int j = 0; j < cols; j++)
            {
                //Figure out the location
                float xPosition = roomWidth * j;
                float zPosition = roomHeight * i;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);

                //Create a new grid at the appropriate location
                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition,
                    Quaternion.identity) as GameObject;

                //Set its parent
                tempRoomObj.transform.parent = this.transform;

                //Give it a meaningful name
                tempRoomObj.name = "Room_" + j + "," + i;

                //Get the room object
                Room tempRoom = tempRoomObj.GetComponent<Room>();

                //Open the doors
                //If we are on the bottom row, open the north door
                if (i == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }//Or if we are on the top row, open the south door
                else if (i == rows - 1)
                {
                    tempRoom.doorSouth.SetActive(false);
                }//Otherwise we are in the middle, open both doors
                else
                {
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }
                //If we are on the first column, open the east door
                if (j == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                }//Or if we are on the last column, open the west door
                else if (j == cols - 1)
                {
                    tempRoom.doorWest.SetActive(false);
                }//Otherwise we are in the middle, open both doors
                else
                {
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }

                //Save it to the grid array
                grid[j, i] = tempRoom;
            }
        }
    }

    //========================================================================

    /*  ================================================================================
    * Tank Spawning functions
    * =================================================================================
    */

    //spawn enemy tanks in random rooms
    public void SpawnEnemies()
    {
        GameObject tempTank;
        MakeList(enemySpawnPoints);

        //spawn one of each of the enemy tanks in a random room at a random waypoint
        foreach (GameObject enemy in enemyTanks)
        {
            TankData tempTankData;
            //create random integer to represent spawnpoint array index
            int index = RandomIndex();
            //instantiate each enemy tank in array in random room at random point
            tempTank = Instantiate(enemy, enemySpawnPoints[index]) as GameObject;

            //Access Tank data and put it in the list
            tempTankData = enemy.GetComponent<TankData>();
            currentEnemyData.Add(tempTankData);

            Debug.Log("Enemy tank added: " + tempTank.gameObject.name);

        }
    }

    

    //spawn one random enemy tank in random room
    public void RespawnEnemy()
    {
        /*GameObject tempTank;
        TankData tempTankData;
        //create random integer to represent spawnpoint array index
        int index = RandomIndex();
        //instantiate new enemy tank in random room at random point
        tempTank = Instantiate(enemy, enemySpawnPoints[index]) as GameObject;

            //Access Tank data and put it in the list
            tempTankData = tempTank.GetComponent<TankData>();
            currentEnemyData.Add(tempTankData);

            Debug.Log("Enemy tank added: " + tempTank.gameObject.name);*/
    }

    void MakeList(Array spawnPts)
    { //make list of all spawnpoints
        SpawnIndex = new List<int>();
        for (int i = 0; i < spawnPts.Length; i++)
        {
            SpawnIndex.Add(i);
        }
    }

    public int RandomIndex()
    { //return random int representing index that hasn't been used yet
      //return -1 if no list made
        if (SpawnIndex == null)
            return -1;
        //return -1 if list is empty
        if (SpawnIndex.Count <= 0)
            return -1;
        //return random index for spawnpoint and remove it so it is not picked again
        int spawnPtIndex = UnityEngine.Random.Range(0, SpawnIndex.Count);
        int pickedIndex = SpawnIndex[spawnPtIndex];
        SpawnIndex.RemoveAt(spawnPtIndex);
        return pickedIndex;
    }

    void SpawnPlayerOne()
    {
        GameObject tempPlayer;
        
        //Create list of spawn points
        MakeList(playerSpawnPoints);
        //create random integer to represent spawnpoint array index
        int index = RandomIndex();
        tempPlayer = Instantiate(playerOnePrefab, playerSpawnPoints[index]) as GameObject;
        tempPlayer.name = "PlayerOne";
        
    }

    void SpawnPlayerTwo()
    {
        GameObject tempPlayer;

        //Create list of spawn points
        MakeList(playerSpawnPoints);
        //create random integer to represent spawnpoint array index
        int index = RandomIndex();
        tempPlayer = Instantiate(playerTwoPrefab, playerSpawnPoints[index]) as GameObject;
        tempPlayer.name = "PlayerTwo";

    }

    public void IsTwoPlayer()
    {
        isTwoPlayerGame = true;
        if (isTwoPlayerGame)
        {
            camera1.rect = new Rect(0, 0, 1, 0.5f);
            camera2.rect = new Rect(0, 0.5f, 1, 0.5f);
        }
        else
        {
            camera1.rect = new Rect(0, 0, 1, 1);
        }
    }

    public void IsOnePlayer()
    {
        isTwoPlayerGame = false;
        camera1.rect = new Rect(0, 0, 1, 1);
    }

    public void GameOver()
    {
        //set game object holding game over screen active
        GameOverScreen.SetActive(true);
        //set game object holding game manager inactive
        this.gameObject.SetActive(false);
        
    }
}
