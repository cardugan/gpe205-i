﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour {

    //Create a List for powerups (can change in size/dynamic array)
    public List<Powerup> powerups;

    //Create variable to access tank data
    private TankData data;

	// Use this for initialization
	void Start () {
        //Initialize powerup list
        powerups = new List<Powerup>();
        data = GetComponent<TankData>();
	}

    // Update is called once per frame
    void Update() {
        //Create a list to hold our expired powerups
        List<Powerup> expiredPowerups = new List<Powerup>();
        //Loop through the powerups on the powerups List
        foreach (Powerup power in powerups)
        {
            //Subtract from the timer
            power.duration -= Time.deltaTime;

            //Assemble list of expired powerups
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);
            }
        }
        //Now use expiredPowerups list to remove expired powerups
        foreach(Powerup power in expiredPowerups)
        {
            power.OnDeactivate(data);
            powerups.Remove(power);
        }
        //Since expiredPowerups List is local, it will "pool" into nothing when this function ends
        //...but we will clear it anyway to learn how to empty a List
        expiredPowerups.Clear();
    }

    public void Add (Powerup powerup)
    {
        //Run OnActivate function of the powerup
        powerup.OnActivate(data);

        //Only add powerups that are NOT permanant to list, to avoid removing any permanent powerups
        if (!powerup.isPermanent)
        {
            powerups.Add(powerup);
        }
    }
}
