﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

    public Powerup powerup;
    private Transform tf;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter (Collider other)
    {
        //Create variable to store other object's PowerupController - if it has one
        PowerupController powCon = other.GetComponent<PowerupController>();

        //If the other object has a PowerupController
        if (powCon != null)
        {
            //Add the powerup to it
            powCon.Add(powerup);

            //Play feedback if it is set
            if (AudioList.instance.powerupPickup != null)
            {
                AudioSource.PlayClipAtPoint(AudioList.instance.powerupPickup, tf.position, 
                    AudioList.instance.sfxVol);
            }

            //Destroy the pickup
            Destroy(gameObject);
        }
    }
}
