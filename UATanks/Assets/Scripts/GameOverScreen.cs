﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverScreen : MonoBehaviour {

    public Text finalText;
    public Text highScoresText;

    public TankData playerOne;
    public TankData playerTwo;

    //Create score list
    public List<ScoreData> scores;

    // Use this for initialization
    void Start () {
        //Get player data
        playerOne = GameObject.Find("PlayerOne").GetComponent<TankData>();
        playerTwo = GameObject.Find("PlayerTwo").GetComponent<TankData>();

        //If list is empty, create default values
        if (scores.Count == 0)
        {
            scores = new List<ScoreData>(3);
        }

        //If 2 player game, display both player scores and add to list
        if (GameManager.instance.isTwoPlayerGame)
        {
            finalText.text = "Player one score: " + playerOne.score +
            "\nPlayer two score: " + playerTwo.score;

            scores.Add(new ScoreData { score = playerOne.score, name = "1" });
            scores.Add(new ScoreData { score = playerTwo.score, name = "2" });
        }
        //If 1 player game, display player score and add to list
        else
        {
            finalText.text = "Player one score: " + playerOne.score;
            scores.Add(new ScoreData { score = playerOne.score, name = "1" });
        }

        //Sort scores, reverse order from high to low
        scores.Sort();
        scores.Reverse();
        //Get top 3 scores
        scores = scores.GetRange(0, 3);
        //Cycle through top 3 and display each to high score text
        for (int i = 0; i < 3; i++)
        {
            highScoresText.text = scores.ToString();
        }
    }


    // Update is called once per frame
    void Update()
    {
    }    
}

