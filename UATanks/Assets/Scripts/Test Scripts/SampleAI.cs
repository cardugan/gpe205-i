﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAI : MonoBehaviour {

    //Create enum for attack mode choices
    public enum AttackMode { Chase, Flee };
    public AttackMode attackMode;
    //Create variable for distance to flee
    public float fleeDistance = 1.0f;

    //Create variables for components
    public Transform target;
    private Transform tf;
    private TankData data;
    private TankMotor motor;

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>();
        data = GetComponent<TankData>();
        motor = GetComponent<TankMotor>();
	}
	
	// Update is called once per frame
	void Update () {
        if (attackMode == AttackMode.Chase) { //Chase: turn toward target and move in that direction
            motor.RotateTowards(target.position, data.turnSpeed);
            motor.Move(data.moveSpeed);
        }
        if (attackMode == AttackMode.Flee) { //Flee: turn away from target and move away
            //The vector from ai to target is the difference between the two positions
            Vector3 vectorToTarget = target.position - tf.position;

            //To get the vector AWAY from target, multiply by -1
            Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

            //Normalize to give it a magnitude of 1
            vectorAwayFromTarget.Normalize();
            //A normalized vector can be multiplied by a length to make a vector of that length
            vectorAwayFromTarget *= fleeDistance;

            //Now add vectorAwayFromTarget to AI's position to get a point to flee to
            Vector3 fleePosition = vectorAwayFromTarget + tf.position;
            motor.RotateTowards(fleePosition, data.turnSpeed);
            motor.Move(data.moveSpeed);
        }
    }
}
