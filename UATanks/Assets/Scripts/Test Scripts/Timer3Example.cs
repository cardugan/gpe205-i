﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer3Example : MonoBehaviour {

    public float timerDelay = 1.0f;
    private float timeUntilNextEvent;

	// Use this for initialization
	void Start () {
        timeUntilNextEvent = timerDelay;
	}
	
	// Update is called once per frame
	void Update () {
        timeUntilNextEvent -= Time.deltaTime;
        if (timeUntilNextEvent <= 0)
        {
            Debug.Log("It's me, three!");
            timeUntilNextEvent = timerDelay;
        }
	}
}
