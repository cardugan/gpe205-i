﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAI : MonoBehaviour {

    //Create enum to choose waypoint loop type
    public enum LoopType { Stop, Loop, PingPong };
    public LoopType loopType;
    //Create bool to check for direction tank is traveling through waypoints
    private bool isPatrolForward = true;

    //Create array for waypoints transforms
    public Transform[] waypoints;
    //Create variable to hold current waypoint
    private int currentWaypoint = 0;
    //Create variable to hold distance that is "close enough" to waypoint & can be changed by designer
    public float closeEnough = 1.0f;

    //Create Transform variable for target
    public Transform target;
    //Create variable for TankMotor script
    private TankMotor motor;
    //Create variable for TankData script
    private TankData data;
    //Create variable for this tank's transform
    private Transform tf;

    //Create variable for Shooter script
    private Shooter shooter;
    //Create timer to track time before next allowed shot
    private float lastShootTime = 0;

    // Runs before Start ()
    void Awake () {
        tf = gameObject.GetComponent<Transform>();
    }

    // Use this for initialization
    void Start () {
        //Set variables to components
        data = GetComponent<TankData>();
        motor = GetComponent<TankMotor>();
        shooter = GetComponent<Shooter>();
    }
	
	// Update is called once per frame
	void Update () {
        //Check if already rotated towards waypoint
        if (motor.RotateTowards(waypoints[currentWaypoint].position, data.turnSpeed))
        {
            //Do nothing! (the tank is turned already where you want it)
        }
        else
        {
            //Move forward
            motor.Move(data.moveSpeed);
        }
        //If we are close to the waypoint
        //Changed Vector3.Distance to squaring each side for optimization
        if (Vector3.SqrMagnitude (waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {
            if (loopType == LoopType.Stop)
            {
                //Advance to next waypoint, if there is one, then stop
                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                }
            }
            else if (loopType == LoopType.Loop)
            {
                //Advance to next waypoint, 
                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                } else
                {
                    //then start over at waypoint 0
                    currentWaypoint = 0;
                }
            }
            else if (loopType == LoopType.PingPong) {
                if (isPatrolForward) { //Check if going forward
                    //Advance to next waypoint, 
                    if (currentWaypoint < waypoints.Length - 1) {
                        currentWaypoint++;
                    } else { //Otherwise reverse direction
                        isPatrolForward = false;
                        currentWaypoint--;
                    }
                } else { //Not going forward
                    if (currentWaypoint > 0) {
                        //Advance to next waypoint going "backward"
                        currentWaypoint--;
                    } else {
                        //When reach waypoint 0, reverse direction
                        isPatrolForward = true;
                        currentWaypoint++;
                    }

                }
            }
        }
        //Limit firing rate and shoot if enough time has passed
        if (Time.time > lastShootTime + data.fireDelay)
        {
            //Shoot target with force and shell destruct time per TankData
            shooter.ShootCannon(data.shotForce, data.shellDestruct);
            //Set delay on timer
            lastShootTime = Time.time;
        }

    }
}
