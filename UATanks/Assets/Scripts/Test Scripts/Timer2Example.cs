﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer2Example : MonoBehaviour {

    public float timerDelay = 1.0f;
    private float lastEventTime;

	// Use this for initialization
	void Start () {
        lastEventTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time >= lastEventTime + timerDelay)
        {
            Debug.Log("It's me, too!");
            lastEventTime = Time.time;
        }
	}
}
