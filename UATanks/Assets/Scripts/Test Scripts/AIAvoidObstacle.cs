﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAvoidObstacle : MonoBehaviour {

    //Create variables for components
    public Transform target;
    private TankData data;
    private TankMotor motor;
    private Transform tf;

    //Create variable for avoidance stage index 
    private int avoidanceStage = 0;
    //Create variables for avoidance timer
    public float avoidanceTime = 2.0f;
    private float exitTime;

    //Create enum for attack mode
    public enum AttackMode { Patrol, Chase, Flee };
    public AttackMode attackMode;


	// Use this for initialization
	void Start () {
        data = GetComponent<TankData>();
        motor = GetComponent<TankMotor>();
        tf = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		if (attackMode == AttackMode.Chase)
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();
            }
        }
	}

    public bool CanMove(float speed)
    {
        //Cast a ray forward in the distance that we sent in
        RaycastHit hit;
        //If our raycast hit something,
        if (Physics.Raycast (tf.position, tf.forward, out hit, speed))
        {//besides the player
            if (!hit.collider.CompareTag("Player"))
            {
                return false;
            }
        }
        //Otherwise, we can move
        return true;
    }

    public void DoAvoidance()
    //Handles obstacle avoidance
    {
        if (avoidanceStage == 1)
        {
            //Rotate left
            motor.Turn(-1 * data.turnSpeed);
            //Check to see if turning cleared the way
            if (CanMove (data.moveSpeed))
            {
                avoidanceStage = 2;

                //Set the timer for the number of seconds to stay in stage 2
                exitTime = avoidanceTime;
            }
            //Otherwise, we'll do this again next turn!
        }
        else if (avoidanceStage == 2)
        {
            //If we can move forward, do so
            if (CanMove(data.moveSpeed))
            {
                //Subtract from timer and move
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);

                //If we have moved for a long enough time, return to chase
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                //Otherwise, can't move forward, so go back to stage 1
                avoidanceStage = 1;
            }
        }
    }

    public void DoChase()
    {
        motor.RotateTowards(target.position, data.turnSpeed);
        //Check if we can move "data.moveSpeed" units away
        //  This distance allows us to look for collisions 1 second into the future
        if (CanMove(data.moveSpeed))
        {
            motor.Move(data.moveSpeed);
        }
        else
        {
            //Can't move, so enter obstacle avoidance stage 1
            avoidanceStage = 1;
        }
    }
}
