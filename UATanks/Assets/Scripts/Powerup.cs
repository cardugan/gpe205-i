﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup {

    //Create variables for powerups
    public float speedModifier;
    public float healthModifier;
    public int numLivesModifier;
    public float fireRateModifier;

    //Create variable for duration of powerups
    public float duration;

    //Create bool for permanent powerups
    public bool isPermanent;

    public void OnActivate (TankData target)
    {//Associate each powerup to its stat in tank data and activate
        target.moveSpeed += speedModifier;
        target.health += healthModifier;
        target.numLives += numLivesModifier;
        target.fireDelay -= fireRateModifier;
    }

    public void OnDeactivate (TankData target)
    {//Deactivate powerup associated with stat in tank data
        target.moveSpeed -= speedModifier;
        target.health -= healthModifier;
        target.numLives -= numLivesModifier;
        target.fireDelay += fireRateModifier;
    }
}
