﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour {

    //Create an array for pickups
    public GameObject[] pickupPrefabs;

    //Create variables for spawn timer
    public float spawnDelay;
    private float nextSpawnTime;
    //Create variable for Spawner transform
    private Transform tf;
    //Create variable for spawned pickup
    GameObject spawnedPickup;

    // Use this for initialization
    void Start () {
        //Set variable to Spawner's Transform
        tf = gameObject.GetComponent<Transform>();
        //Set timer
        nextSpawnTime = Time.time + spawnDelay;
	}
	
	// Update is called once per frame
	void Update () {
       
        //If there is nothing spawned
        if (spawnedPickup == null)
        {
            //And if its time to spawn pickup
            if (Time.time > nextSpawnTime)
            {
                //Spawn it and set the next time
                spawnedPickup = Instantiate(RandomPickupPrefab(), tf.position, Quaternion.identity) as GameObject;
                //Set its parent
                spawnedPickup.transform.parent = this.transform;
                foreach (Pickup spawnedPickup in GameManager.instance.spawnedPickups)
                {
                    GameManager.instance.spawnedPickups.Add(spawnedPickup);
                }
                nextSpawnTime = Time.time + spawnDelay;
            }
        }
        else
        {
            //Otherwise the spawned pickup still exists, so postpone the next spawn
            nextSpawnTime = Time.time + spawnDelay;
        }
	}

    //Returns random pickup
    public GameObject RandomPickupPrefab ()
    {
        return pickupPrefabs[Random.Range(0, pickupPrefabs.Length)];
    }
}
